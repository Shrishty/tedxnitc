This site is based on Drupal.

IMPORTANT STEPS TO MAKE THIS SITE WORK ON YOUR PC
-------------------------------------------------

Do following steps on terminal

* cd /var/www/
* git init --bare tedx.git
* git clone https://username@bitbucket.org/Shrishty/tedxnitc tedx_dev
* cd tedx_dev
* git pull all_branches (at present shrishty and tedx)
* in your browser open "localhost/tedx_dev"
* do continue until you get error msg on the browser

* follow the steps for drupal installation
  
  * sudo su
  * type the password 
  * cd /var/www/tedx_dev/sites/default
  * cp default.settings.php settings.php
  * chmod a+w settings.php
  * mkdir files
  * chmod a+w files
  * go to the browser again and refresh the page, errors will be gone
  * now for creating db
    
    * go to browser and open "localhost/phpmyadmin"
    * sign in as root 
    * go to privilages
    * go to create new user
    * give user name (remember it)
    * choose host as "localhost"
    * put a password (remember it)
    * choose "create database with same name and grant all privilages"
    * then create user

  * go back to "localhost/tedx_dev"
  * put the database name (same name as above)
  * put user name (same)
  * put password (same as above)
  * fill other things
  * choose continue/next
  * fill the relevent details
  * drupal website will start downloading
  * then open your website

THIS IS TO BE DONE ALWAYS TO SEE THE UPDATED VERSION
----------------------------------------------------

* back to terminal
* if not pulled before "git pull origin 'all the branches'".
* cd /var/www/tedx_dev
* drush en backup_migrate
* on your browser "localhost/tedx_dev"
* in the admin menu on the top 
* go to configuration
* then system
* then restore tab
* then click choose file
* now a go to "/var/www/tedx_dev/Bacups/
* and choose "TEDx.............mysql.gz"
* refresh your browser
* complete!!
   
